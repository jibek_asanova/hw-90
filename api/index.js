const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid');
const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnection = {};
let pixels = [];

app.ws('/canvas', (ws) => {
    const id = nanoid();
    activeConnection[id] = ws;

    ws.send(JSON.stringify({type: 'PREV_MESSAGE', pixels}));

    ws.on('message', msg => {
        const decoded = JSON.parse(msg);
        pixels = decoded.pixels;

        switch (decoded.type) {
            case 'DRAW_IMAGE':
                Object.keys(activeConnection).forEach(key => {
                    const connection = activeConnection[key];
                    connection.send(JSON.stringify({
                            type: 'NEW_MESSAGE',
                            pixels: decoded.pixels
                        }
                    ))
                });
                break;
            default:
                console.log('Unknown type: ', decoded.type)
        }
    });

    ws.on('close', () => {
        console.log(`Client disconnected! id = ${id}`);
        delete activeConnection[id];
    })

});

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
})