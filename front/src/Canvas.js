import React, {useState, useRef, useEffect} from 'react';


const Canvas = () => {
    const [state, setState] = useState({
        mouseDown: false,
        pixelsArray: []
    });

    const canvas = useRef(null);
    const ws = useRef(null);

    useEffect(() => {
        ws.current = new WebSocket('ws://localhost:8000/canvas');
        ws.current.onmessage = event => {
            const decoded = JSON.parse(event.data);
            const context = canvas.current.getContext('2d');
            const imageData = context.createImageData(1, 1);

            const d = imageData.data;
            d[0] = 0;
            d[1] = 0;
            d[2] = 0;
            d[3] = 255;

            switch (decoded.type) {
                case 'NEW_MESSAGE':
                    setState(prev => ({...prev,
                        pixelsArray: decoded.pixels
                    }));
                    for (let i = 0; i < decoded.pixels.length; i++) {
                        context.putImageData(imageData, decoded.pixels[i].x, decoded.pixels[i].y);
                    }
                    break;
                case 'PREV_MESSAGE':
                    setState(prev => ({...prev,
                        pixelsArray: decoded.pixels
                    }));
                    for (let i = 0; i < decoded.pixels.length; i++) {
                        context.putImageData(imageData, decoded.pixels[i].x, decoded.pixels[i].y);
                    }
                    break;
                default:
                    console.log('Unknown type: ', decoded.type);
            }
        }
    }, []);


    const canvasMouseMoveHandler = event => {
        if (state.mouseDown) {
            event.persist();
            const clientX = event.clientX;
            const clientY = event.clientY;
            setState(prevState => {
                return {
                    ...prevState,
                    pixelsArray: [...prevState.pixelsArray, {
                        x: clientX,
                        y: clientY
                    }]
                };
            });
            const context = canvas.current.getContext('2d');
            const imageData = context.createImageData(1, 1);
            const d = imageData.data;


            d[0] = 0;
            d[1] = 0;
            d[2] = 0;
            d[3] = 255;

            context.putImageData(imageData, event.clientX, event.clientY);
        }


    };

    const mouseDownHandler = () => {
        setState(prev => ({...prev,  mouseDown: true}));
    };

    const mouseUpHandler = () => {
        ws.current.send(JSON.stringify({
                type: 'DRAW_IMAGE',
                pixels: state.pixelsArray,
            }
        ));

        setState(prev => ({...prev, mouseDown: false, pixelsArray: []}));
    };

    return (
        <div>
            <canvas
                ref={canvas}
                style={{border: '1px solid black'}}
                width={800}
                height={600}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
                onMouseMove={canvasMouseMoveHandler}
            />
        </div>
    );
};

export default Canvas;